def Solve(N, A):
    for i in A:
        if i == 0:
            return i
    return min((abs(x), x) for x in A)[0]


N = int(input())

A = list(map(int, input().split()))

out_ = Solve(N, A)

print(out_)
