def small_palindrome(s):
    return str(chr(min([ord(c) for c in s])))