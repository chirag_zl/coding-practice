from collections import defaultdict


def SmallestSubSing(S):
    n = len(S)
    dist_count = len(set([x for x in S]))

    count, start, start_index, min_len = 0, 0, -1, 9999999999
    curr_count = defaultdict(lambda: 0)
    for j in range(n):
        curr_count[S[j]] += 1
        if curr_count[S[j]] == 1:
            count += 1

        if count == dist_count:
            while curr_count[S[start]] > 1:
                if curr_count[S[start]] > 1:
                    curr_count[S[start]] -= 1
                start += 1

            len_window = j - start + 1
            if min_len > len_window:
                min_len = len_window
                start_index = start
    return len(S[start_index: start_index + min_len])


S = input()

out_ = SmallestSubSing(S)
print(out_)
